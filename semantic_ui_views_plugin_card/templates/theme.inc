<?php

/**
 * @file
 * Themes variables of semantic_ui_views_plugin_card.
 */

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_semantic_ui_views_plugin_card(&$vars) {
  $view                 = &$vars['view'];

  $model_style_card         = $vars['options']['model_style_card'];
  $special_card             = $vars['options']['special_card'];
  $special_card_field       = $vars['options']['special_card_field'];
  $columns                  = $vars['options']['columns'];

  $path_field               = $vars['options']['path_field'];
  $image_field              = $vars['options']['image_field'];
  $heading_field            = $vars['options']['heading_field'];
  $meta_field               = $vars['options']['meta_field'];
  $body_field               = $vars['options']['body_field'];
  $extra_content1           = $vars['options']['extra_content1'];
  $extra_content2           = $vars['options']['extra_content2'];

  $vars['model_style_card'] = $view->style_plugin->options['model_style_card'];
  $vars['special_card']     = $view->style_plugin->options['special_card'];
  $vars['class_heading_field']  = $view->style_plugin->options['class_heading_field'];
  $vars['class_body_field']     = $view->style_plugin->options['class_body_field'];
  $vars['class_extra_content1'] = $view->style_plugin->options['class_extra_content1'];
  $vars['class_extra_content2'] = $view->style_plugin->options['class_extra_content2'];
  $vars['dimmers'] = $special_card === 'dimmers';
  $vars['reveal']  = $special_card === 'reveal';

  $vars['items']            = array();

  foreach ($view->result as $key => $field) {
    $vars['items'][] = array(
      'special_card_field'  => isset($view->field[$special_card_field]) ? $view->style_plugin->get_field($key, $special_card_field) : NULL,
      'path_field'          => isset($view->field[$path_field]) ? $view->style_plugin->get_field($key, $path_field) : NULL,
      'image_field'         => isset($view->field[$image_field]) ? $view->style_plugin->get_field($key, $image_field) : NULL,
      'heading_field'       => isset($view->field[$heading_field]) ? $view->style_plugin->get_field($key, $heading_field) : NULL,
      'meta_field'          => isset($view->field[$meta_field]) ? $view->style_plugin->get_field($key, $meta_field) : NULL,
      'body_field'          => isset($view->field[$body_field]) ? $view->style_plugin->get_field($key, $body_field) : NULL,
      'extra_content1'        => isset($view->field[$extra_content1]) ? $view->style_plugin->get_field($key, $extra_content1) : NULL,
      'extra_content2'        => isset($view->field[$extra_content2]) ? $view->style_plugin->get_field($key, $extra_content2) : NULL,
    );
  }

  unset($vars['classes_array']);
  $vars['classes_array'][] = 'ui';
  $vars['wrapper_class']   = 'card';
  $vars['wrapper_prefix']  = '<div class="' . $vars['wrapper_class'] . '">';
  $vars['wrapper_suffix']  = '</div>';

  $vars['image_wrapper_class']   = 'image';
  $vars['image_wrapper_prefix']  = '<div class="' . $vars['image_wrapper_class'] . '">';
  $vars['image_wrapper_suffix']  = '</div>';

  if ($model_style_card == 'doubling') {
    $vars['classes_array'][] = $columns;
    $vars['classes_array'][] = 'doubling cards';
  }
  if ($model_style_card == 'fluid_card') {
    $vars['classes_array'][] = $columns;
    $vars['classes_array'][] = 'columns';
    $vars['classes_array'][] = 'grid';
    $vars['wrapper_class'] = 'column';
    $vars['wrapper_prefix']  = '<div class="' . $vars['wrapper_class'] . '">' . '<div class="ui fluid card">';
    $vars['wrapper_suffix']  = '</div></div>';
  }

  if ($special_card == 'dimmers') {
    unset($vars['classes_array']);
    if ($model_style_card == 'doubling') {
      $vars['classes_array'][] = 'doubling';
    }
    $vars['classes_array'][] = 'ui';
    $vars['classes_array'][] = $columns;
    $vars['classes_array'][] = 'special';
    $vars['classes_array'][] = 'cards';
    $vars['wrapper_class'] = 'card';
    $vars['wrapper_prefix']  = '<div class="' . $vars['wrapper_class'] . '">';
    $vars['wrapper_suffix']  = '</div>';
    drupal_add_js(drupal_get_path('module', 'semantic_ui_views_plugin_card') . '/js/semantic-ui-views-plugin-card.js');
  }
  if ($vars['reveal']) {
    $vars['image_wrapper_class'] = 'ui fade reveal image';
    $vars['image_wrapper_prefix']  = '<div class="' . $vars['image_wrapper_class'] . '">';
    $vars['image_wrapper_suffix']  = '</div>';
    drupal_add_css(drupal_get_path('module', 'semantic_ui_views_plugin_card') . '/css/semantic-ui-views-plugin-card.css');
  }
}

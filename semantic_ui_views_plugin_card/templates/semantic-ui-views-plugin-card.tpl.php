<?php

/**
 * @file
 * Display Semantic UI Views Card.
 */
?>
<?php if (!empty($title)) : ?>
  <?php print $title; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>">
  <?php foreach ($items as $key => $item): ?>
    <?php print $wrapper_prefix; ?>
    <?php print $image_wrapper_prefix; ?>
    <?php if (($reveal) || ($dimmers)): ?>
      <?php if ($reveal): ?>
        <div class="visible content">
          <?php print $item['image_field']; ?>
        </div>
        <?php if ($item['special_card_field']): ?>
          <div class="hidden content">
            <?php print $item['special_card_field']; ?>
          </div>
        <?php endif ?>
      <?php endif ?>
      <?php if ($dimmers): ?>
        <div class="ui dimmer">
          <div class="content">
            <div class="center">
              <?php if ($item['special_card_field']): ?>
                <?php print $item['special_card_field']; ?>
              <?php endif ?>
            </div>
          </div>
        </div>
        <?php print $item['image_field']; ?>
      <?php endif ?>
    <?php else: ?>
      <?php if ($item['image_field']): ?>
        <?php print $item['image_field']; ?>
      <?php endif ?>
    <?php endif ?>

    <?php print $image_wrapper_suffix; ?>

    <div class="content">
      <?php if ($item['heading_field']): ?>
        <div class="header">
          <?php print $item['heading_field']; ?>
        </div>
      <?php endif ?>

      <?php if ($item['body_field']): ?>
        <div class="description">
          <?php print $item['body_field']; ?>
        </div>
      <?php endif ?>
    </div>
    <?php if (($item['extra_content2']) || ($item['extra_content1'])): ?>
      <div class="extra content">
        <?php if ($item['extra_content2']): ?>
          <div class="right floated <?php print $class_extra_content2; ?>">
            <?php print $item['extra_content2']; ?>
          </div>
        <?php endif ?>
        <?php if ($item['extra_content1']): ?>
          <div class="<?php print $class_extra_content1; ?>">
            <?php print $item['extra_content1']; ?>
          </div>
        <?php endif ?>
      </div>
    <?php endif ?>
    <?php print $wrapper_suffix; ?>
  <?php endforeach ?>
</div>

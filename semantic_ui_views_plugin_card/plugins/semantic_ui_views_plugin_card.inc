<?php

/**
 * @file
 * Definitions of semantic_ui_views_plugin_card.
 */

/**
 * Plugin outpout each item in SemanticUI Card style.
 *
 * @ingroup views_style_plugins
 */
class semantic_ui_views_plugin_card extends views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['class_views_top'] = array(
      'default' => NULL,
    );
    $options['class_views_heading'] = array(
      'default' => NULL,
    );
    $options['class_views_content'] = array(
      'default' => NULL,
    );
    $options['class_views_footer'] = array(
      'default' => NULL,
    );
    $options['model_style_card'] = array(
      'default' => NULL,
    );;
    $options['columns'] = array(
      'default' => NULL,
    );
    $options['special_card'] = array(
      'default' => NULL,
    );
    $options['special_card_field'] = array(
      'default' => NULL,
    );
    $options['path_field'] = array(
      'default' => NULL,
    );
    $options['image_field'] = array(
      'default' => NULL,
    );
    $options['heading_field'] = array(
      'default' => NULL,
    );
    $options['class_heading_field'] = array(
      'default' => NULL,
    );
    $options['meta_field'] = array(
      'default' => NULL,
    );
    $options['body_field'] = array(
      'default' => NULL,
    );
    $options['class_body_field'] = array(
      'default' => NULL,
    );
    $options['extra_content1'] = array(
      'default' => NULL,
    );
    $options['class_extra_content1'] = array(
      'default' => NULL,
    );
    $options['extra_content2'] = array(
      'default' => NULL,
    );
    $options['class_extra_content2'] = array(
      'default' => NULL,
    );

    return $options;
  }

  /**
   * Semantic UI Views Card Form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    foreach (element_children($form['grouping']) as $key => $value) {
      if (!empty($form['grouping'][$key]['field']['#options']) && is_array($form['grouping'][$key]['field']['#options'])) {
        $options = array_merge($options, $form['grouping'][$key]['field']['#options']);
      }
    }

    $form['customize_top_views'] = array(
      '#type' => 'checkbox',
      '#title' => t('Customize Top Views Class') ,
      '#description' => t('Customize Views top header, content...') ,
      '#default_value' => $this->options['customize_top_views'],
    );

    $form['class_views_root'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Views Class') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[customize_top_views]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['class_views_root'],
    );

    $form['class_views_root']['class_views_top'] = array(
      '#title' => t('Views TOP class') ,
      '#description' => t('The class to provide on each top wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_top'],
    );

    $form['class_views_root']['class_views_heading'] = array(
      '#title' => t('Views Heading class') ,
      '#description' => t('The class to provide on each heading wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_heading'],
    );

    $form['class_views_root']['class_views_content'] = array(
      '#title' => t('Views Content class') ,
      '#description' => t('The class to provide on each content wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_content'],
    );

    $form['class_views_root']['class_views_footer'] = array(
      '#title' => t('Views Footer class') ,
      '#description' => t('The class to provide on each footer wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_footer'],
    );

    $form['model_style_card'] = array(
      '#type' => 'select',
      '#title' => t('Card Outpout Style') ,
      '#options' => array(
        'fluid_card' => t('Fluid Card Variation') ,
        'doubling' => t('Doubling Variation') ,
      ) ,
      '#required' => TRUE,
      '#default_value' => $this->options['model_style_card'],
    );

    $form['columns'] = array(
      '#type' => 'select',
      '#title' => t('Number of Columns') ,
      '#options' => array(
        'one' => t('One') ,
        'two' => t('Two') ,
        'three' => t('Three') ,
        'four' => t('Four') ,
        'five' => t('Five') ,
        'six' => t('Six') ,
        'seven' => t('Seven') ,
        'eight' => t('Eight') ,
        'nine' => t('Nine') ,
        'ten' => t('Ten') ,
        'eleven' => t('Eleven') ,
        'twelve' => t('Twelve') ,
        'thirten' => t('Thirten') ,
        'fourten' => t('Fourten') ,
        'fiveten' => t('Fiveten') ,
        'sixten' => t('Sixten') ,
      ) ,
      '#default_value' => $this->options['columns'],
    );

    $form['special_card'] = array(
      '#type' => 'select',
      '#title' => t('Special Effect Card Reveal or Dimmers') ,
      '#empty_value' => '',
      '#options' => array(
        'reveal' => t('Reveal Variation') ,
        'dimmers' => t('Dimmers Variation') ,
      ) ,
      '#default_value' => $this->options['special_card'],
    );

    $form['path_field'] = array(
      '#type' => 'select',
      '#title' => t('Path field') ,
      '#options' => $options,
      '#default_value' => $this->options['path_field'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[model_style_card]"]' => array(
            '!value' => 'link_card',
          ) ,
        ) ,
      ) ,
      '#description' => t('Select the field that will be used as the path.') ,
    );

    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('Image field') ,
      '#options' => $options,
      '#default_value' => $this->options['image_field'],
      '#description' => t('Select the field that will be used as the image.') ,
    );

    $form['special_card_field'] = array(
      '#type' => 'select',
      '#title' => t('Special field') ,
      '#options' => $options,
      '#default_value' => $this->options['special_card_field'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[special_card]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
      '#description' => t('Select the field that will be used for reveal or dimmers addons. You nead disable all default class.') ,
    );

    $form['heading_field'] = array(
      '#type' => 'select',
      '#title' => t('Heading field') ,
      '#options' => $options,
      '#default_value' => $this->options['heading_field'],
      '#description' => t('Select the field that will be used as the heading.') ,
    );

    $form['class_heading_field'] = array(
      '#title' => t('Heading class') ,
      '#description' => t('The class to provide on each heading field.') ,
      '#type' => 'textfield',
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[heading_field]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['class_heading_field'],
    );

    $form['meta_field'] = array(
      '#type' => 'select',
      '#title' => t('Meta field') ,
      '#options' => $options,
      '#default_value' => $this->options['meta_field'],
      '#description' => t('Select the field that will be used as the meta.') ,
    );

    $form['body_field'] = array(
      '#type' => 'select',
      '#title' => t('Content Body field') ,
      '#options' => $options,
      '#default_value' => $this->options['body_field'],
      '#description' => t('Select the field that will be used as the body.') ,
    );

    $form['class_body_field'] = array(
      '#title' => t('Body class') ,
      '#description' => t('The class to provide on each heading field.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_body_field'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[body_field]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );

    $form['extra_content1'] = array(
      '#type' => 'select',
      '#title' => t('Extra Content Field') ,
      '#options' => $options,
      '#default_value' => $this->options['extra_content1'],
      '#description' => t('Select the field that will be used in class Extra Content 1.') ,
    );

    $form['class_extra_content1'] = array(
      '#title' => t('Extra Content 1 Suplemantary Class') ,
      '#description' => t('The class to provide on Extra content 1.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_extra_content1'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[extra_content1]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );

    $form['extra_content2'] = array(
      '#type' => 'select',
      '#title' => t('Extra Content 2 Field') ,
      '#options' => $options,
      '#default_value' => $this->options['extra_content2'],
      '#description' => t('Select the field that will be used in class Extra Content 2.') ,
    );

    $form['class_extra_content2'] = array(
      '#title' => t('Extra Content 2 Suplemantary Class') ,
      '#description' => t('The class to provide on Extra content 2.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_extra_content2'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[extra_content2]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );
  }

}

<?php

/**
 * @file
 * Semantic_ui_views_plugin_card data handler.
 */

/**
 * Implements hook_views_plugins().
 */
function semantic_ui_views_plugin_card_views_plugins() {
  $module_path = drupal_get_path('module', 'semantic_ui_views_plugin_card');

  return array(
    'style' => array(
      'semantic_ui_views_plugin_card' => array(
        'title' => t('Semantic UI Card') ,
        'help' => t('Semantic UI Views Card') ,
        'path' => $module_path . '/plugins',
        'handler' => 'SemanticUiViewsPluginCard',
        'parent' => 'default',
        'theme' => 'semantic_ui_views_plugin_card',
        'theme path' => $module_path . '/templates',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ) ,
    ) ,
  );
}

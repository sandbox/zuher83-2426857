/**
 * @file
 * Add Semantic UI dimmers function.
 */

(function ($) {
  Drupal.behaviors.semanticUiViewsPluginCard = {
    attach: function(context) {
      $('.special.cards').dimmer({
        on: 'hover'
      });
    }
  };
})(jQuery);

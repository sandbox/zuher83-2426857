<?php

/**
 * @file
 * Semantic_ui_views_plugin_feed data handler.
 */

/**
 * Implements hook_views_plugins().
 */
function semantic_ui_views_plugin_feed_views_plugins() {
  $module_path = drupal_get_path('module', 'semantic_ui_views_plugin_feed');

  return array(
    'style' => array(
      'semantic_ui_views_plugin_feed' => array(
        'title' => t('Semantic UI Feed') ,
        'help' => t('Semantic UI Feed') ,
        'path' => $module_path . '/plugins',
        'handler' => 'semantic_ui_views_plugin_feed',
        'parent' => 'default',
        'theme' => 'semantic_ui_views_plugin_feed',
        'theme path' => $module_path . '/templates',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'semantic-ui-views-plugin-feed',
      ) ,
    ) ,
  );
}

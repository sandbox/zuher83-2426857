<?php
/**
 * @file
 * Themes variables of semantic_ui_views_plugin_feed.
 */

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_semantic_ui_views_plugin_feed(&$vars) {
  $view                   = &$vars['view'];

  $image_field            = $vars['options']['image_field'];
  $summary_user           = $vars['options']['summary_user'];
  $summary_message        = $vars['options']['summary_message'];
  $summary_date           = $vars['options']['summary_date'];
  $content_text           = $vars['options']['content_text'];
  $class_content_text     = $vars['options']['class_content_text'];
  $extra_images           = $vars['options']['extra_images'];
  $meta                   = $vars['options']['meta'];
  $class_meta             = $vars['options']['class_meta'];

  $vars['message_module'] = $vars['options']['use_message_module'];
  $vars['items']          = array();

  foreach (array_keys($vars['rows']) as $key) {
    $vars['items'][] = array(
      'image_field'         => isset($view->field[$image_field]) ? $view->style_plugin->get_field($key, $image_field) : NULL,
      'summary_user'        => isset($view->field[$summary_user]) ? $view->style_plugin->get_field($key, $summary_user) : NULL,
      'summary_message'     => isset($view->field[$summary_message]) ? $view->style_plugin->get_field($key, $summary_message) : NULL,
      'summary_date'        => isset($view->field[$summary_date]) ? $view->style_plugin->get_field($key, $summary_date) : NULL,
      'content_text'        => isset($view->field[$content_text]) ? $view->style_plugin->get_field($key, $content_text) : NULL,
      'class_content_text'  => $class_content_text,
      'extra_images'        => isset($view->field[$extra_images]) ? $view->style_plugin->get_field($key, $extra_images) : NULL,
      'meta'                => isset($view->field[$meta]) ? $view->style_plugin->get_field($key, $meta) : NULL,
      'class_meta'          => $class_meta,
    );
  }
}

<?php

/**
 * @file
 * Semantic UI Views Feed display feed list.
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif ?>
<div class="ui feed <?php print $classes; ?>">
  <?php foreach ($items as $key => $item): ?>
    <div class="event">
      <?php if ($item['image_field']): ?>
        <div class="label">
          <?php print $item['image_field']; ?>
        </div>
      <?php endif ?>
      <?php if ($message_module): ?>
        <div class="content <?php print $item['class_content_text'] ?>">
          <?php print $item['content_text']; ?>
          <?php if ($item['meta']): ?>
            <div class="meta <?php print $item['class_meta']; ?>">
              <?php print $item['meta']; ?>
            </div>
          <?php endif ?>
        </div>
      <?php else: ?>
        <div class="content <?php print $item['class_content_text']; ?>">
          <div class="summary">
            <?php if ($item['summary_user']): ?>
              <?php print $item['summary_user']; ?>
            <?php endif ?>
            <?php if ($item['summary_message']): ?>
              <?php print $item['summary_message']; ?>
            <?php endif ?>
            <?php if ($item['summary_date']): ?>
              <div class="date">
                <?php print $item['summary_date']; ?>
              </div>
            <?php endif ?>
          </div>
          <?php if ($item['content_text']): ?>
            <div class="extra text">
              <?php print $item['content_text']; ?>
            </div>
          <?php endif ?>
          <?php if ($item['extra_images']): ?>
            <div class="extra images">
              <?php print $item['extra_images']; ?>
            </div>
          <?php endif ?>
          <?php if ($item['meta']): ?>
            <div class="meta <?php print $item['class_meta']; ?>">
              <?php print $item['meta']; ?>
            </div>
          <?php endif ?>
        </div>
      <?php endif ?>
    </div>
  <?php endforeach ?>
</div>

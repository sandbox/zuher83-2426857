<?php

/**
 * @file
 * Definition of Semantic UI FEED Style.
 */

/**
 * Plugin outpout each item in SemanticUI Feed style.
 *
 * @ingroup views_style_plugins
 */
class semantic_ui_views_plugin_feed extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['customize_top_views'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_top'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_heading'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_content'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_footer'] = array(
      'default' => NULL,
    );
    $options['use_message_module'] = array(
      'default' => NULL,
    );
    $options['image_field'] = array(
      'default' => NULL,
    );
    $options['summary_user'] = array(
      'default' => NULL,
    );
    $options['summary_message'] = array(
      'default' => NULL,
    );
    $options['summary_date'] = array(
      'default' => NULL,
    );
    $options['content_text'] = array(
      'default' => NULL,
    );
    $options['class_content_text'] = array(
      'default' => NULL,
    );
    $options['heading_field'] = array(
      'default' => NULL,
    );
    $options['heading_style'] = array(
      'default' => NULL,
    );
    $options['class_heading_field'] = array(
      'default' => NULL,
    );
    $options['extra_images'] = array(
      'default' => NULL,
    );
    $options['meta'] = array(
      'default' => NULL,
    );
    $options['class_meta'] = array(
      'default' => NULL,
    );
    return $options;
  }

  /**
   * Build Semantic UI Feed options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    foreach (element_children($form['grouping']) as $key => $value) {
      if (!empty($form['grouping'][$key]['field']['#options']) && is_array($form['grouping'][$key]['field']['#options'])) {
        $options = array_merge($options, $form['grouping'][$key]['field']['#options']);
      }
    }

    $form['customize_top_views'] = array(
      '#type' => 'checkbox',
      '#title' => t('Customize Top Views Class') ,
      '#description' => t('Customize Views top header, content...') ,
      '#default_value' => $this->options['customize_top_views'],
    );

    $form['class_views_root'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Views Class') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[customize_top_views]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['class_views_root'],
    );

    $form['class_views_root']['class_views_top'] = array(
      '#title' => t('Views TOP class') ,
      '#description' => t('The class to provide on each top wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_top'],
    );

    $form['class_views_root']['class_views_heading'] = array(
      '#title' => t('Views Heading class') ,
      '#description' => t('The class to provide on each heading wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_heading'],
    );

    $form['class_views_root']['class_views_content'] = array(
      '#title' => t('Views Content class') ,
      '#description' => t('The class to provide on each content wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_content'],
    );

    $form['class_views_root']['class_views_footer'] = array(
      '#title' => t('Views Footer class') ,
      '#description' => t('The class to provide on each footer wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_footer'],
    );

    $form['use_message_module'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Message module?') ,
      '#description' => t('If you use with Message module, you need add rights class in directly message type entity. You must use only "Image field" "Content text Field" and optionally "Meta". Read README.TXT more informations') ,
      '#default_value' => $this->options['use_message_module'],
    );

    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('Image field') ,
      '#options' => $options,
      '#default_value' => $this->options['image_field'],
      '#description' => t('Select the field that will be used as the image.') ,
    );

    $form['summary_user'] = array(
      '#type' => 'select',
      '#title' => t('User Name in summary appear') ,
      '#options' => $options,
      '#default_value' => $this->options['summary_user'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[use_message_module]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['summary_message'] = array(
      '#type' => 'select',
      '#title' => t('Summary Message appear after user') ,
      '#options' => $options,
      '#default_value' => $this->options['summary_message'],
      '#description' => t('Select the field that will be used as the heading.') ,
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[use_message_module]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['summary_date'] = array(
      '#type' => 'select',
      '#title' => t('Summary Date') ,
      '#options' => $options,
      '#default_value' => $this->options['summary_date'],
      '#description' => t('Select the field that will be used as the heading.') ,
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[use_message_module]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['content_text'] = array(
      '#type' => 'select',
      '#title' => t('Content Text Field') ,
      '#options' => $options,
      '#default_value' => $this->options['content_text'],
      '#description' => t('Select the field that will be used as the body.') ,
    );

    $form['class_content_text'] = array(
      '#title' => t('Body class') ,
      '#description' => t('The class to provide on each heading field.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_content_text'],
    );

    $form['extra_images'] = array(
      '#type' => 'select',
      '#title' => t('Extra Images Field') ,
      '#options' => $options,
      '#default_value' => $this->options['extra_images'],
      '#description' => t('Select the field that will be added extra images after content.') ,
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[use_message_module]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['meta'] = array(
      '#type' => 'select',
      '#title' => t('Meta Field') ,
      '#options' => $options,
      '#default_value' => $this->options['meta'],
      '#description' => t('Select the field that will be used in footer.') ,
    );

    $form['class_meta'] = array(
      '#title' => t('Footer class') ,
      '#description' => t('Add extra class in meta section.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_meta'],
    );
  }

}

<?php
/**
 * @file
 * Themes variables of semantic_ui_views.
 */

/**
 * Implementats hook theme.
 */
function template_preprocess_semantic_ui_views(&$vars) {
  $view                = &$vars['view'];
  $vars['class_views_top']          = $view->style_plugin->options['class_views_root']['class_views_top'];
  $vars['class_views_heading']      = $view->style_plugin->options['class_views_root']['class_views_heading'];
  $vars['class_views_content']      = $view->style_plugin->options['class_views_root']['class_views_content'];
  $vars['class_views_footer']       = $view->style_plugin->options['class_views_root']['class_views_footer'];
}

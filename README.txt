Semantic UI Views
-------------

Introduction:
-------------
Semantic UI Views integrates the features of the theme Semantic UI for Views
module.
Created views fast with extended configuration to work with the "Semantic UI"
theme.

Features:
-------------
 - GRID
 - ITEM
 - CARD
 - FEED

Requierements:
-------------
 - Views (https://www.drupal.org/project/views)
 - Semantic UI (https://www.drupal.org/project/semanticui)

Installation
-------------
 * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

Other :
-------------

Message Module: (https://www.drupal.org/project/message)
-------------
* If you use MESSAGE module, you can make your feed with the plugin
"Semantic UI Feed". You simply configure the user's image and place
"Message: Render message" in the "CONTENT" field.
* Then the message types configuration menu individually configure the output
with the tokens from the <div class="content"> .

Example to output a comment message, I set my type of message like this:
  <div class="summary">
  [message:user:name] posted new comment
    <div class="date">[message:timestamp:since] ago</div>
  </div>
  <div class="extra text">
    [message:field-message-comment-ref:comment-teaser]
  </div>
  <div class="meta">
    <a href="[message:field-message-comment-ref:node:url]/
#comment-[message:field-message-comment-ref:cid]">
<i class="comments outline icon"></i> Reply</a>
  </div>

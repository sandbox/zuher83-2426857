<?php

/**
 * @file
 * Definition of semantic_ui_views_plugin_list.
 */

/**
 * Plugin outpout each item in SemanticUI List style.
 *
 * @ingroup views_style_plugins
 */
class semantic_ui_views_plugin_list extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['customize_top_views'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_top'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_heading'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_content'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_footer'] = array(
      'default' => NULL,
    );
    $options['list_type'] = array(
      'default' => NULL,
    );
    $options['interverted'] = array(
      'default' => NULL,
    );
    $options['relaxed'] = array(
      'default' => NULL,
    );
    $options['use_image'] = array(
      'default' => NULL,
    );
    $options['divided'] = array(
      'default' => NULL,
    );
    $options['celled'] = array(
      'default' => NULL,
    );
    $options['animated'] = array(
      'default' => NULL,
    );
    $options['item_size'] = array(
      'default' => NULL,
    );
    $options['content_alignement'] = array(
      'default' => NULL,
    );
    $options['image_field'] = array(
      'default' => NULL,
    );
    $options['header_field'] = array(
      'default' => NULL,
    );
    $options['meta'] = array(
      'default' => NULL,
    );
    $options['content_field'] = array(
      'default' => NULL,
    );
    $options['extra_field'] = array(
      'default' => NULL,
    );
    $options['extra_field_class'] = array(
      'default' => NULL,
    );
    $options['class_content_field'] = array(
      'default' => NULL,
    );
    $options['class_header_field'] = array(
      'default' => NULL,
    );
    return $options;
  }

  /**
   * Form options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    foreach (element_children($form['grouping']) as $key => $value) {
      if (!empty($form['grouping'][$key]['field']['#options']) && is_array($form['grouping'][$key]['field']['#options'])) {
        $options = array_merge($options, $form['grouping'][$key]['field']['#options']);
      }
    }

    $form['customize_top_views'] = array(
      '#type' => 'checkbox',
      '#title' => t('Customize Top Views Class') ,
      '#description' => t('Customize Views top header, content...') ,
      '#default_value' => $this->options['customize_top_views'],
    );

    $form['class_views_root'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Views Class') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[customize_top_views]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['class_views_root'],
    );

    $form['class_views_root']['class_views_top'] = array(
      '#title' => t('Views TOP class') ,
      '#description' => t('The class to provide on each top wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_top'],
    );

    $form['class_views_root']['class_views_heading'] = array(
      '#title' => t('Views Heading class') ,
      '#description' => t('The class to provide on each heading wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_heading'],
    );

    $form['class_views_root']['class_views_content'] = array(
      '#title' => t('Views Content class') ,
      '#description' => t('The class to provide on each content wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_content'],
    );

    $form['class_views_root']['class_views_footer'] = array(
      '#title' => t('Views Footer class') ,
      '#description' => t('The class to provide on each footer wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_footer'],
    );

    $form['list_type'] = array(
      '#type' => 'select',
      '#title' => t('Select List Type') ,
      '#description' => t('Add horizontal border') ,
      '#required' => TRUE,
      '#options' => array(
        'vertical' => t('Vertical') ,
        'horizontal' => t('Horizontal') ,
      ) ,
      '#default_value' => $this->options['list_type'],
    );

    $form['interverted'] = array(
      '#type' => 'checkbox',
      '#title' => t('Interverted Style') ,
      '#description' => t('Set interverted style') ,
      '#default_value' => $this->options['interverted'],
    );

    $form['relaxed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Relaxed Style') ,
      '#description' => t('Set to relaxed items') ,
      '#default_value' => $this->options['relaxed'],
    );

    $form['use_image'] = array(
      '#type' => 'checkbox',
      '#title' => t('Are to use image field ?') ,
      '#description' => t('Sort item list with image field') ,
      '#default_value' => $this->options['use_image'],
    );

    $form['divided'] = array(
      '#type' => 'checkbox',
      '#title' => t('Divided Style') ,
      '#description' => t('Add horizontal border') ,
      '#default_value' => $this->options['divided'],
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[celled]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['celled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Celled Style') ,
      '#description' => t('Set celled style') ,
      '#default_value' => $this->options['celled'],
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[divided]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
    );

    $form['animated'] = array(
      '#type' => 'checkbox',
      '#title' => t('Item Animated') ,
      '#description' => t('Add animated style to item') ,
      '#default_value' => $this->options['animated'],
    );

    $form['item_size'] = array(
      '#type' => 'select',
      '#title' => t('Outpout Item Size') ,
      '#empty_value' => '',
      '#options' => array(
        'mini' => t('Mini') ,
        'tiny' => t('Tiny') ,
        'small' => t('Small') ,
        'large' => t('Large') ,
        'big' => t('Big') ,
        'huge' => t('Huge') ,
        'massive' => t('Massive') ,
      ) ,
      '#default_value' => $this->options['item_size'],
    );

    $form['content_alignement'] = array(
      '#type' => 'select',
      '#title' => t('Content Alignement') ,
      '#empty_value' => '',
      '#options' => array(
        'top' => t('Top') ,
        'middle' => t('Middle') ,
        'bottom' => t('Bottom') ,
      ) ,
      '#default_value' => $this->options['content_alignement'],
    );

    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('Image field') ,
      '#options' => $options,
      '#default_value' => $this->options['image_field'],
      '#description' => t('Select the field that will be used as the image.') ,
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[use_image]"]' => array(
            'checked' => FALSE,
          ) ,
        ) ,
      ) ,
    );

    $form['header_field'] = array(
      '#type' => 'select',
      '#title' => t('Heading field') ,
      '#options' => $options,
      '#default_value' => $this->options['header_field'],
      '#description' => t('Select the field that will be display as the heading section.') ,
    );

    $form['class_header_field'] = array(
      '#title' => t('Extra heading class') ,
      '#description' => t('Add more class to heading section.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_header_field'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[header_field]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );

    $form['meta'] = array(
      '#type' => 'select',
      '#title' => t('Meta field') ,
      '#options' => $options,
      '#default_value' => $this->options['meta'],
      '#description' => t('Select the field that will be display as the meta section.') ,
    );

    $form['content_field'] = array(
      '#type' => 'select',
      '#title' => t('Content field') ,
      '#options' => $options,
      '#default_value' => $this->options['content_field'],
      '#description' => t('Select the field that will be used as the content.') ,
    );

    $form['class_content_field'] = array(
      '#title' => t('Extra content class') ,
      '#description' => t('Add more class to content section.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_content_field'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[content_field]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );

    $form['extra_field'] = array(
      '#type' => 'select',
      '#title' => t('Extra field') ,
      '#options' => $options,
      '#default_value' => $this->options['extra_field'],
      '#description' => t('Select the field that will be used as the extra section.') ,
    );

    $form['extra_field_class'] = array(
      '#title' => t('Extra Field class') ,
      '#description' => t('Add more class to extra section.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['extra_field_class'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[extra_field]"]' => array(
            'value' => '',
          ) ,
        ) ,
      ) ,
    );
  }

}

<?php
/**
 * @file
 * Semantic_ui_views_plugin_list data handler.
 */

/**
 * Implements hook_views_plugins().
 */
function semantic_ui_views_plugin_list_views_plugins() {
  $module_path = drupal_get_path('module', 'semantic_ui_views_plugin_list');

  return array(
    'style' => array(
      'semantic_ui_views_plugin_list' => array(
        'title' => t('Semantic UI List') ,
        'help' => t('Semantic UI List') ,
        'path' => $module_path . '/plugins',
        'handler' => 'semantic_ui_views_plugin_list',
        'parent' => 'default',
        'theme' => 'semantic_ui_views_plugin_list',
        'theme path' => $module_path . '/templates',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ) ,
    ) ,
  );
}

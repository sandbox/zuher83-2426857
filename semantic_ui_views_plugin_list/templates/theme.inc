<?php
/**
 * @file
 * Themes variables of semantic_ui_views_plugin_list.
 */

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_semantic_ui_views_plugin_list(&$vars) {
  $view                = &$vars['view'];

  $image_field         = $vars['options']['image_field'];
  $header_field        = $vars['options']['header_field'];
  $meta                = $vars['options']['meta'];
  $content_field       = $vars['options']['content_field'];
  $extra_field         = $vars['options']['extra_field'];

  $options             = $view->style_plugin->options;
  $interverted         = $options['interverted'];
  $relaxed             = $options['relaxed'];
  $divided             = $options['divided'];
  $celled              = $options['celled'];
  $animated            = $options['animated'];
  $item_size           = $options['item_size'];
  $content_alignement  = $options['content_alignement'];

  $class_header_field  = $options['class_header_field'];
  $class_content_field = $options['class_content_field'];
  $extra_field_class   = $options['extra_field_class'];

  $vars['items']       = array();

  foreach (array_keys($vars['rows']) as $key) {
    $vars['items'][] = array(
      'image_field'   => isset($view->field[$image_field]) ? $view->style_plugin->get_field($key, $image_field) : NULL,
      'header_field'  => isset($view->field[$header_field]) ? $view->style_plugin->get_field($key, $header_field) : NULL,
      'meta'          => isset($view->field[$meta]) ? $view->style_plugin->get_field($key, $meta) : NULL,
      'content_field' => isset($view->field[$content_field]) ? $view->style_plugin->get_field($key, $content_field) : NULL,
      'extra_field'   => isset($view->field[$extra_field]) ? $view->style_plugin->get_field($key, $extra_field) : NULL,
    );
  }

  unset($vars['classes_array']);
  $vars['classes_array'][] = 'ui list';

  if ($divided) {
    $vars['classes_array'][] = 'divided';
  }
  if ($item_size) {
    $vars['classes_array'][] = $item_size;
  }
  if ($relaxed) {
    $vars['classes_array'][] = 'relaxed';
  }
  if ($celled) {
    $vars['classes_array'][] = 'celled';
  }
  if ($animated) {
    $vars['classes_array'][] = 'animated';
  }

  $vars['class_header_field']    = 'header' . $class_header_field;
  $vars['class_content_field']   = 'description' . $class_content_field;
  $vars['extra_field_class']     = 'extra' . $extra_field_class;
  $vars['content_wrapper_class'] = 'content';

  $vars['interverted_prefix'] = '';
  $vars['interverted_suffix'] = '';
  if ($interverted) {
    $vars['interverted_prefix'] = '<div class="ui inverted segment">';
    $vars['interverted_suffix'] = '</div>';
  }
  if ($content_alignement) {
    $vars['content_wrapper_class'] .= ' ' . $content_alignement . ' ' . 'aligned';
  }
}

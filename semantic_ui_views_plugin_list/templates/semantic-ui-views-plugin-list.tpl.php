<?php

/**
 * @file
 * Semantic UI Views List allows to oupout List style items.
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php if ($interverted_prefix): ?>
  <?php print $interverted_prefix ?>
<?php endif ?>
<div class="<?php print $classes; ?>">
  <?php foreach ($items as $key => $item): ?>
    <div class="item">
      <?php if ($item['image_field']): ?>
          <?php print $item['image_field']; ?>
      <?php endif ?>
      <div class="<?php print $content_wrapper_class; ?>">

        <?php if ($item['header_field']): ?>
        <div class="<?php print $class_header_field; ?>">
          <?php print $item['header_field'] ?>
        </div>
        <?php endif ?>

        <?php if ($item['meta']): ?>
        <div class="meta">
          <?php print $item['meta'] ?>
        </div>
        <?php endif ?>

        <?php if ($item['content_field']): ?>
          <div class="<?php print $class_content_field; ?>">
            <?php print $item['content_field'] ?>
          </div>
        <?php endif ?>
        <?php if ($item['extra_field']): ?>
          <div class="<?php print $extra_field_class; ?>">
            <?php print $item['extra_field'] ?>
          </div>
        <?php endif ?>

      </div>
    </div>
  <?php endforeach ?>
</div>
<?php if ($interverted_suffix): ?>
  <?php print $interverted_suffix ?>
<?php endif ?>

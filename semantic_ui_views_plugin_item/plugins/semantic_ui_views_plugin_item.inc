<?php

/**
 * @file
 * Definition of semantic_ui_views_plugin_item.
 */

/**
 * Plugin outpout each item in SemanticUI Item style.
 *
 * @ingroup views_style_plugins
 */
class semantic_ui_views_plugin_item extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['grouping_label'] = array(
      'default' => NULL,
    );
    $options['grouping_label_root']['label_style'] = array(
      'default' => NULL,
    );
    $options['grouping_label_root']['label_attached_position'] = array(
      'default' => 'top_left',
    );
    $options['grouping_label_root']['label_color'] = array(
      'default' => NULL,
    );
    $options['grouping_label_root']['label_icon'] = array(
      'default' => NULL,
    );
    $options['grouping_label_root']['label_icon_name'] = array(
      'default' => NULL,
    );
    $options['customize_top_views'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_top'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_heading'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_content'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_footer'] = array(
      'default' => NULL,
    );
    $options['use_image'] = array(
      'default' => NULL,
    );
    $options['divided'] = array(
      'default' => NULL,
    );
    $options['image_size'] = array(
      'default' => NULL,
    );
    $options['content_alignement'] = array(
      'default' => NULL,
    );
    $options['image_field'] = array(
      'default' => NULL,
    );
    $options['header_field'] = array(
      'default' => NULL,
    );
    $options['class_header_field'] = array(
      'default' => NULL,
    );
    $options['meta'] = array(
      'default' => NULL,
    );
    $options['content_field'] = array(
      'default' => NULL,
    );
    $options['class_content_field'] = array(
      'default' => NULL,
    );
    $options['extra_field'] = array(
      'default' => NULL,
    );
    $options['extra_field_class'] = array(
      'default' => NULL,
    );
    return $options;
  }

  /**
   * Buld Semantic UI Item options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if (isset($form['grouping'])) {
      $options = array();
      foreach (element_children($form['grouping']) as $key => $value) {
        if (!empty($form['grouping'][$key]['field']['#options']) && is_array($form['grouping'][$key]['field']['#options'])) {
          $options = array_merge($options, $form['grouping'][$key]['field']['#options']);
        }
      }

      // Add styling option for grouping.
      $form['grouping_label'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add ribbon label for title grouping') ,
        '#description' => t('Customize grouping outpout with label...') ,
        '#default_value' => $this->options['grouping_label'],
      );

      $form['grouping_label_root'] = array(
        '#type' => 'fieldset',
        '#title' => t('Settings grouping') ,
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
        '#default_value' => $this->options['grouping_label_root'],
      );

      $form['grouping_label_root']['label_style'] = array(
        '#type' => 'select',
        '#title' => t('Label Style') ,
        '#description' => t('See <a src="http://semantic-ui.com/elements/label.html">http://semantic-ui.com/elements/label.html</a>') ,
        '#empty_value' => '',
        '#options' => array(
          'ribbon' => t('Ribbon') ,
        ) ,
        '#default_value' => $this->options['grouping_label_root']['label_style'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label_root][grouping_label]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      $form['grouping_label_root']['label_attached_position'] = array(
        '#type' => 'select',
        '#title' => t('Label attached position') ,
        '#description' => t('See <a src="http://semantic-ui.com/elements/label.html#attached">http://semantic-ui.com/elements/label.html#attached</a>') ,
        '#options' => array(
          'top_right' => t('Top right') ,
          'top_left' => t('Top left') ,
        ) ,
        '#default_value' => $this->options['grouping_label_root']['label_attached_position'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label_root][grouping_label]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      $form['grouping_label_root']['label_color'] = array(
        '#type' => 'select',
        '#title' => t('Label color') ,
        '#empty_value' => '',
        '#options' => array(
          'black' => t('black') ,
          'yellow' => t('yellow') ,
          'green' => t('green') ,
          'blue' => t('blue') ,
          'orange' => t('orange') ,
          'purple' => t('purple') ,
          'red' => t('red') ,
          'teal' => t('teal') ,
        ) ,
        '#default_value' => $this->options['grouping_label_root']['label_color'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label_root][grouping_label]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      $form['grouping_label_root']['label_icon'] = array(
        '#type' => 'checkbox',
        '#title' => t('Insert icon?') ,
        '#description' => t('Add icon in label') ,
        '#default_value' => $this->options['grouping_label_root']['label_icon'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label_root][grouping_label]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      $form['grouping_label_root']['label_icon_name'] = array(
        '#title' => t('Icon name') ,
        '#description' => t('Insert only icon name, see <a src="http://semantic-ui.com/elements/icon.html">http://semantic-ui.com/elements/icon.html</a>).') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['grouping_label_root']['label_icon_name'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[grouping_label_root][label_icon]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      // Customize views class.
      $form['customize_top_views'] = array(
        '#type' => 'checkbox',
        '#title' => t('Customize Top Views Class') ,
        '#description' => t('Customize Views top header, content...') ,
        '#default_value' => $this->options['customize_top_views'],
      );

      $form['class_views_root'] = array(
        '#type' => 'fieldset',
        '#title' => t('Setting Views Class') ,
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[customize_top_views]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
        '#default_value' => $this->options['class_views_root'],
      );

      $form['class_views_root']['class_views_top'] = array(
        '#title' => t('Views TOP class') ,
        '#description' => t('The class to provide on each top wrapper.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['class_views_root']['class_views_top'],
      );

      $form['class_views_root']['class_views_heading'] = array(
        '#title' => t('Views Heading class') ,
        '#description' => t('The class to provide on each heading wrapper.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['class_views_root']['class_views_heading'],
      );

      $form['class_views_root']['class_views_content'] = array(
        '#title' => t('Views Content class') ,
        '#description' => t('The class to provide on each content wrapper.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['class_views_root']['class_views_content'],
      );

      $form['class_views_root']['class_views_footer'] = array(
        '#title' => t('Views Footer class') ,
        '#description' => t('The class to provide on each footer wrapper.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['class_views_root']['class_views_footer'],
      );

      // Build style with options.
      $form['use_image'] = array(
        '#type' => 'checkbox',
        '#title' => t('Are tou use image field ?') ,
        '#description' => t('Sort item list with image field') ,
        '#default_value' => $this->options['use_image'],
      );

      $form['divided'] = array(
        '#type' => 'checkbox',
        '#title' => t('Divided Style') ,
        '#description' => t('Add horizontal border') ,
        '#default_value' => $this->options['divided'],
      );

      $form['image_size'] = array(
        '#type' => 'select',
        '#title' => t('Outpout Image Size') ,
        '#empty_value' => '',
        '#options' => array(
          'mini' => t('Mini') ,
          'tiny' => t('Tiny') ,
          'small' => t('Small') ,
          'large' => t('Large') ,
          'big' => t('Big') ,
          'huge' => t('Huge') ,
          'massive' => t('Massive') ,
        ) ,
        '#default_value' => $this->options['image_size'],
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[use_image]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      );

      $form['content_alignement'] = array(
        '#type' => 'select',
        '#title' => t('Content Alignement') ,
        '#empty_value' => '',
        '#options' => array(
          'top' => t('Top') ,
          'middle' => t('Middle') ,
          'bottom' => t('Bottom') ,
        ) ,
        '#default_value' => $this->options['content_alignement'],
      );

      $form['image_field'] = array(
        '#type' => 'select',
        '#title' => t('Image field') ,
        '#options' => $options,
        '#default_value' => $this->options['image_field'],
        '#description' => t('Select the field that will be used as the image.') ,
        '#states' => array(
          'invisible' => array(
            ':input[name="style_options[use_image]"]' => array(
              'checked' => FALSE,
            ) ,
          ) ,
        ) ,
      );

      $form['header_field'] = array(
        '#type' => 'select',
        '#title' => t('Heading field') ,
        '#options' => $options,
        '#default_value' => $this->options['header_field'],
        '#description' => t('Select the field that will be display as the heading section.') ,
      );

      $form['class_header_field'] = array(
        '#title' => t('Extra heading class') ,
        '#description' => t('Add more class to heading section.') ,
        '#type' => 'textfield',
        '#states' => array(
          'visible' => array(
            ':input[name="header_field"]' => array(
              '!value' => '',
            ) ,
          ) ,
        ) ,
        '#default_value' => $this->options['class_header_field'],
      );

      $form['meta'] = array(
        '#type' => 'select',
        '#title' => t('Meta field') ,
        '#options' => $options,
        '#default_value' => $this->options['meta'],
        '#description' => t('Select the field that will be display as the meta section.') ,
      );

      $form['content_field'] = array(
        '#type' => 'select',
        '#title' => t('Content field') ,
        '#options' => $options,
        '#default_value' => $this->options['content_field'],
        '#description' => t('Select the field that will be used as the content.') ,
      );
      $form['class_content_field'] = array(
        '#title' => t('Extra content class') ,
        '#description' => t('Add more class to content section.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['class_content_field'],
        '#states' => array(
          'visible' => array(
            ':input[name="content_field"]' => array(
              '!value' => '',
            ) ,
          ) ,
        ) ,
      );

      $form['extra_field'] = array(
        '#type' => 'select',
        '#title' => t('Extra field') ,
        '#options' => $options,
        '#default_value' => $this->options['extra_field'],
        '#description' => t('Select the field that will be used as the extra section.') ,
      );

      $form['extra_field_class'] = array(
        '#title' => t('Extra Field class') ,
        '#description' => t('Add more class to extra section.') ,
        '#type' => 'textfield',
        '#default_value' => $this->options['extra_field_class'],
        '#states' => array(
          'visible' => array(
            ':input[name="extra_field"]' => array(
              '!value' => '',
            ) ,
          ) ,
        ) ,
      );
    }
  }

}

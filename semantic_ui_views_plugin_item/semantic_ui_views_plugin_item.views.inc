<?php

/**
 * @file
 * Semantic_ui_views_plugin_item data handler.
 */

/**
 * Implements hook_views_plugins().
 */
function semantic_ui_views_plugin_item_views_plugins() {
  $module_path = drupal_get_path('module', 'semantic_ui_views_plugin_item');

  return array(
    'style' => array(
      'semantic_ui_views_plugin_item' => array(
        'title' => t('Semantic UI Item') ,
        'help' => t('Semantic UI Item') ,
        'path' => $module_path . '/plugins',
        'handler' => 'semantic_ui_views_plugin_item',
        'parent' => 'default',
        'theme' => 'semantic_ui_views_plugin_item',
        'theme path' => $module_path . '/templates',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ) ,
    ) ,
  );
}

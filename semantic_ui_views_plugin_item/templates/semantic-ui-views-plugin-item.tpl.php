<?php

/**
 * @file
 * Default simple view template to display a item.
 */
?>
<?php if (!empty($title)) : ?>
  <?php print $label_wrapper_prefix; ?>
    <?php if (!empty($icons)): ?>
      <?php print $icons; ?>
    <?php endif; ?>
    <?php print $title; ?>
  <?php print $label_wrapper_suffix; ?>
<?php endif; ?>

<div class="<?php print $classes; ?>">
  <?php foreach ($items as $key => $item): ?>
    <div class="item">
      <?php if ($item['image_field']): ?>
        <div class="<?php print $image_class; ?>">
          <?php print $item['image_field']; ?>
        </div>
      <?php endif ?>
      <div class="<?php print $content_wrapper_class; ?>">
        <?php if ($item['header_field']): ?>
          <div class="<?php print $class_header_field; ?>">
            <?php print $item['header_field']; ?>
          </div>
        <?php endif ?>

        <?php if ($item['meta']): ?>
          <div class="meta">
            <?php print $item['meta']; ?>
          </div>
        <?php endif ?>

        <?php if ($item['content_field']): ?>
          <div class="<?php print $class_content_field; ?>">
            <?php print $item['content_field']; ?>
          </div>
        <?php endif ?>
        <?php if ($item['extra_field']): ?>
          <div class="<?php print $extra_field_class; ?>">
            <?php print $item['extra_field']; ?>
          </div>
        <?php endif ?>
      </div>
    </div>
  <?php endforeach ?>
</div>

<?php

/**
 * @file
 * Themes variables of semantic_ui_views_plugin_item.
 */

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_semantic_ui_views_plugin_item(&$vars) {
  $view = $vars['view'];
  $options = $view->style_plugin->options;

  $label_root = $options['grouping_label_root'];
  $grouping_label = $options['grouping_label'];
  $label_style = $label_root['label_style'];
  $label_attached_pos = $label_root['label_attached_position'];
  $label_color = $label_root['label_color'];
  $label_icon_name = $label_root['label_icon_name'];

  $image_field = $vars['options']['image_field'];
  $header_field = $vars['options']['header_field'];
  $meta = $vars['options']['meta'];
  $content_field = $vars['options']['content_field'];
  $extra_field = $vars['options']['extra_field'];

  $use_image = $options['use_image'];
  $divided = $options['divided'];
  $image_size = $options['image_size'];
  $content_alignement = $options['content_alignement'];
  $class_header_field = $options['class_header_field'];
  $class_content_field = $options['class_content_field'];
  $extra_field_class = $options['extra_field_class'];

  $vars['items'] = array();

  foreach (array_keys($vars['rows']) as $key) {
    $vars['items'][] = array(
      'image_field' => isset($view->field[$image_field]) ? $view->style_plugin->get_field($key, $image_field) : NULL,
      'header_field' => isset($view->field[$header_field]) ? $view->style_plugin->get_field($key, $header_field) : NULL,
      'meta' => isset($view->field[$meta]) ? $view->style_plugin->get_field($key, $meta) : NULL,
      'content_field' => isset($view->field[$content_field]) ? $view->style_plugin->get_field($key, $content_field) : NULL,
      'extra_field' => isset($view->field[$extra_field]) ? $view->style_plugin->get_field($key, $extra_field) : NULL,
    );
  }

  unset($vars['classes_array']);
  $vars['classes_array'][] = 'ui items';

  if ($divided) {
    $vars['classes_array'][] = 'divided';
  }

  $vars['class_header_field'] = 'header' . $class_header_field;
  $vars['class_content_field'] = 'description' . $class_content_field;
  $vars['extra_field_class'] = 'extra' . $extra_field_class;
  $vars['image_class'] = array();
  $vars['image_class'][] = 'ui';
  $vars['image_class'][] = 'image';
  $vars['content_wrapper_class'] = 'content';

  if ($use_image) {
    if ($image_size) {
      $vars['image_class'][] = $image_size;
    }
  }

  if ($content_alignement) {
    $vars['content_wrapper_class'] .= ' ' . $content_alignement . ' ' . 'aligned';
  }

  $label_wrapper_class = array();
  if ($grouping_label) {
    $label_wrapper_class[] = 'ui';

    if (!empty($label_color)) {
      $label_wrapper_class[] = $label_color;
    }

    if ($label_style === 'ribbon') {
      if ($label_attached_pos === 'top_right') {
        $label_wrapper_class[] = 'right';
      }
    }
    $label_wrapper_class[] = $label_style;
    $label_wrapper_class[] = 'label';

    if (!empty($label_icon_name)) {
      $vars['icons'] = '<i class="' . $label_icon_name . ' icon"></i>';
    }
  }

  $vars['image_class'] = implode(' ', $vars['image_class']);
  $label_wrapper_class = implode(' ', $label_wrapper_class);
  $vars['label_wrapper_prefix'] = '<div class=" ' . $label_wrapper_class . '">';
  $vars['label_wrapper_suffix'] = '</div>';
}

<?php

/**
 * @file
 * Semantic UI Views Grid allows to oupout responsive grids.
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif ?>

<?php if ($style_grid_type == 'rows'): ?>
  <?php if ($grid_wrapper_prefix): ?>
    <?php print $grid_wrapper_prefix; ?>
  <?php endif ?>
  <?php foreach ($items as $row): ?>
    <?php if ($wrapper_prefix): ?>
      <?php print $wrapper_prefix; ?>
    <?php endif ?>
    <?php foreach ($row['content'] as $column): ?>
      <div class="<?php print $item_class; ?>">
        <?php if ($segment_prefix): ?>
          <?php print $segment_prefix; ?>
        <?php endif ?>
        <?php print $column['content']; ?>
        <?php if ($segment_suffix): ?>
          <?php print $segment_suffix; ?>
        <?php endif ?>
      </div>
    <?php endforeach ?>

    <?php if ($wrapper_suffix): ?>
      <?php print $wrapper_suffix; ?>
    <?php endif ?>
  <?php endforeach ?>
  <?php if ($grid_wrapper_suffix): ?>
    <?php print $grid_wrapper_suffix; ?>
  <?php endif ?>
<?php endif ?>

<?php if ($style_grid_type != 'rows'): ?>
  <?php if ($grid_wrapper_prefix): ?>
    <?php print $grid_wrapper_prefix; ?>
  <?php endif ?>
  <?php if ($wrapper_prefix): ?>
    <?php print $wrapper_prefix; ?>
  <?php endif ?>
  <?php foreach ($rows as $id => $row): ?>
    <div class="<?php print $item_class; ?>">
      <?php if ($segment_prefix): ?>
        <?php print $segment_prefix; ?>
      <?php endif ?>
      <?php print $row; ?>
      <?php if ($segment_suffix): ?>
        <?php print $segment_suffix; ?>
      <?php endif ?>
    </div>
  <?php endforeach; ?>
  <?php if ($wrapper_suffix): ?>
    <?php print $wrapper_suffix; ?>
  <?php endif ?>
  <?php if ($grid_wrapper_suffix): ?>
    <?php print $grid_wrapper_suffix; ?>
  <?php endif ?>
<?php endif ?>

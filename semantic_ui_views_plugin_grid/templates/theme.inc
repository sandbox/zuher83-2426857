<?php

/**
 * @file
 * Themes variables of semantic_ui_views_plugin_grid.
 */

/**
 * Implementation of template preprocess for the view.
 */
function template_preprocess_semantic_ui_views_plugin_grid(&$vars) {
  $view = $vars['view'];
  $options = $view->style_plugin->options;
  $horizontal = ($options['alignment'] === 'horizontal');
  $vars['style_grid_type'] = $options['style_grid_type'];

  // Definition Html Class From Options.
  $divided = $options['variations_set']['divided_grid'];
  $equal_height = $options['row_options']['equal_height'];
  $celled_grid = $options['row_options']['celled_grid'];
  $grid_alignement = $options['global_options']['grid_alignement'];
  $segment = $options['variations_set']['segment'];
  $vars['grid_wrapper_class'] = '';
  $vars['responsive_grids_class'] = '';
  $vars['global_options_class'] = '';
  $vars['variations_class'] = '';
  $vars['item_class'] = '';
  $vars['segment_prefix'] = '';
  $vars['segment_suffix'] = '';
  $grid_class = array();
  $wrapper_class = array();
  $grid_class[] = 'ui';
  $grid_class[] = 'grid';

  if ($options['variations_set']['relaxed']) {
    $grid_class[] = 'relaxed';
  }
  if ($options['variations_set']['padded']) {
    $grid_class[] = 'padded';
  }
  if ($divided) {
    $grid_class[] = 'divided';
  }
  if ($grid_alignement) {
    $grid_class[] = $grid_alignement;
    $grid_class[] = 'aligned';
  }
  if ($segment) {
    $vars['segment_prefix'] = '<div class="ui segment">';
    $vars['segment_suffix'] = '</div>';
  }

  // Rows Outpout Style.
  if ($vars['style_grid_type'] === 'rows') {
    $gridcolumns = $options['global_options']['columns'];
    $rowcolumns = array(
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirten',
      14 => 'fourten',
      15 => 'fiveten',
      16 => 'sixten',
    );

    $col_key = array_search($gridcolumns, $rowcolumns);
    $columns = $col_key;
    $vars['items'] = _semantic_ui_views_split_rows($vars, $columns, $horizontal);

    // Set class.
    $wrapper_class[] = $gridcolumns;
    $wrapper_class[] = 'column';
    $wrapper_class[] = 'row';
    $vars['item_class'] = 'column';

    if ($equal_height) {
      $wrapper_class[] = 'equal height';
    }

    if ($celled_grid) {
      if ($options['row_options']['type_celled_grid'] == 'celled') {
        $grid_class[] = 'celled';
      }
      if ($options['row_options']['type_celled_grid'] == 'internal') {
        $grid_class[] = 'internally celled';
      }
    }

    $wrapper_class = implode(' ', $wrapper_class);
    $grid_class = implode(' ', $grid_class);

    // HTML Outpout.
    $vars['grid_wrapper_prefix'] = '<div class="' . $grid_class . '">';
    $vars['grid_wrapper_suffix'] = '</div>';
    $vars['wrapper_prefix'] = '<div class="' . $wrapper_class . '">';
    $vars['wrapper_suffix'] = '</div>';

    if ($divided or $equal_height) {
      if ($equal_height && $divided) {
        $vars['grid_wrapper_prefix'] = '<div class="ui ' . $gridcolumns . ' column grid test1">';
        $vars['wrapper_prefix'] = '<div class="equal height divided row">';
        $vars['item_class'] = 'column';
        $vars['wrapper_suffix'] = '</div>';
        $vars['grid_wrapper_suffix'] = '</div>';
      }
    }
  }

  // Responsive Grid Outpout.
  if ($vars['style_grid_type'] != 'rows') {

    $responsive_grids = $options['responsive_options']['responsive_grids'];
    $columns = $options['global_options']['columns'];
    $customize_by_device = $options['global_options']['customize_by_device'];
    $mobile_device = $options['global_options']['columns_mobile'];
    $tablet_device = $options['global_options']['columns_tablet'];

    $vars['grid_wrapper_prefix'] = '';
    $vars['grid_wrapper_suffix'] = '';
    $vars['wrapper_prefix'] = '';
    $vars['wrapper_suffix'] = '';

    if ($responsive_grids) {
      $grid_class[] = $responsive_grids;
    }

    if ($columns) {
      if ($customize_by_device) {
        $wrapper_class[] = $columns;
        $wrapper_class[] = 'wide';
        $wrapper_class[] = 'computer';

        if (isset($mobile_device)) {
          $wrapper_class[] = $mobile_device;
          $wrapper_class[] = 'wide';
          $wrapper_class[] = 'mobile';
        }
        if (isset($mobile_device)) {
          $wrapper_class[] = $tablet_device;
          $wrapper_class[] = 'wide';
          $wrapper_class[] = 'tablet';
        }
      }
      else {
        $grid_class[] = $columns . ' column';
      }
    }

    $grid_class = implode(' ', $grid_class);
    $wrapper_class = implode(' ', $wrapper_class);
    $vars['item_class'] = 'column';

    // Html build.
    if ($responsive_grids == 'wide') {
      $vars['grid_wrapper_prefix'] = '<div class="' . $grid_class . '">';
      $vars['grid_wrapper_suffix'] = '</div>';
      if ($customize_by_device) {
        $vars['grid_wrapper_prefix'] = '<div class="ui grid">';
        $vars['item_class'] = $wrapper_class . ' column';
      }
    }

    if ($responsive_grids == 'stackable') {
      $vars['grid_wrapper_prefix'] = '<div class="' . $grid_class . '">';
      $vars['grid_wrapper_suffix'] = '</div>';
    }

    if ($responsive_grids == 'doubling') {
      $vars['grid_wrapper_prefix'] = '<div class="' . $grid_class . '">';
      $vars['grid_wrapper_suffix'] = '</div>';
    }
  }
}

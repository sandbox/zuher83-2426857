<?php

/**
 * @file
 * Definition of Semantic UI Grid Style.
 */

/**
 * Plugin outpout each item in SemanticUI Grid style.
 *
 * @ingroup views_style_plugins
 */
class semantic_ui_views_plugin_grid extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['alignment'] = array(
      'default' => 'horizontal',
    );
    $options['customize_top_views'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_top'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_heading'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_content'] = array(
      'default' => NULL,
    );
    $options['class_views_root']['class_views_footer'] = array(
      'default' => NULL,
    );
    $options['style_grid_type'] = array(
      'default' => NULL,
    );
    $options['row_options']['equal_height'] = array(
      'default' => NULL,
    );
    $options['row_options']['celled_grid'] = array(
      'default' => NULL,
    );
    $options['row_options']['type_celled_grid'] = array(
      'default' => NULL,
    );
    $options['responsive_options']['responsive_grids'] = array(
      'default' => NULL,
    );
    $options['global_options']['columns'] = array(
      'default' => NULL,
    );
    $options['global_options']['customize_by_device'] = array(
      'default' => NULL,
    );
    $options['global_options']['columns_mobile'] = array(
      'default' => NULL,
    );
    $options['global_options']['columns_tablet'] = array(
      'default' => NULL,
    );
    $options['global_options']['grid_alignement'] = array(
      'default' => NULL,
    );
    $options['global_options']['vertical_alignement'] = array(
      'default' => NULL,
    );
    $options['variations_set']['segment'] = array(
      'default' => NULL,
    );
    $options['variations_set']['padded'] = array(
      'default' => NULL,
    );
    $options['variations_set']['relaxed'] = array(
      'default' => NULL,
    );
    $options['variations_set']['divided_grid'] = array(
      'default' => NULL,
    );
    $options['variations_set']['verticaly_grid'] = array(
      'default' => NULL,
    );
    return $options;
  }

  /**
   * Semantic UI Views Grid Options Form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();

    foreach (element_children($form['grouping']) as $key => $value) {
      if (!empty($form['grouping'][$key]['field']['#options']) && is_array($form['grouping'][$key]['field']['#options'])) {
        $options = array_merge($options, $form['grouping'][$key]['field']['#options']);
      }
    }

    $form['customize_top_views'] = array(
      '#type' => 'checkbox',
      '#title' => t('Customize Top Views Class') ,
      '#description' => t('Customize Views top header, content...') ,
      '#default_value' => $this->options['customize_top_views'],
    );

    $form['class_views_root'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Views Class') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[customize_top_views]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['class_views_root'],
    );

    $form['class_views_root']['class_views_top'] = array(
      '#title' => t('Views TOP class') ,
      '#description' => t('The class to provide on each top wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_top'],
    );

    $form['class_views_root']['class_views_heading'] = array(
      '#title' => t('Views Heading class') ,
      '#description' => t('The class to provide on each heading wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_heading'],
    );

    $form['class_views_root']['class_views_content'] = array(
      '#title' => t('Views Content class') ,
      '#description' => t('The class to provide on each content wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_content'],
    );

    $form['class_views_root']['class_views_footer'] = array(
      '#title' => t('Views Footer class') ,
      '#description' => t('The class to provide on each footer wrapper.') ,
      '#type' => 'textfield',
      '#default_value' => $this->options['class_views_root']['class_views_footer'],
    );

    $form['style_grid_type'] = array(
      '#type' => 'select',
      '#title' => t('Grid Type') ,
      '#description' => t('A grid can have rows divisions only between simple celled or internal rows') ,
      '#empty_value' => '',
      '#required' => TRUE,
      '#options' => array(
        'responsive' => t('Responsive') ,
        'rows' => t('Rows') ,
      ) ,
      '#default_value' => $this->options['style_grid_type'],
    );

    // Rows Grid Settings.
    $form['row_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting More Rows Options') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[style_grid_type]"]' => array(
            'value' => 'rows',
          ) ,
        ) ,
      ) ,
    );

    $form['row_options']['alignment'] = array(
      '#type' => 'radios',
      '#title' => t('Alignment') ,
      '#options' => array(
        'horizontal' => t('Horizontal') ,
      ) ,
      '#description' => t('Horizontal alignment will place items starting in the upper left and moving right. Vertical alignment will place items starting in the upper left and moving down.') ,
      '#default_value' => $this->options['row_options']['alignment'],
    );

    $form['row_options']['equal_height'] = array(
      '#type' => 'checkbox',
      '#title' => t('Equal Hight Grid') ,
      '#description' => t('A row can specify that it is equal height so all of its columns appear the length of its longest column.') ,
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[row_options][celled_grid]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
        'invisible' => array(
          ':input[name="style_options[style_grid_type]"]' => array(
            'value' => 'responsive',
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['row_options']['equal_height'],
    );

    $form['row_options']['celled_grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Celled Grid') ,
      '#description' => t('A grid can have rows divided into cells') ,
      '#empty_value' => '',
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[variations_set][divided_grid]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['row_options']['celled_grid'],
    );

    $form['row_options']['type_celled_grid'] = array(
      '#type' => 'select',
      '#title' => t('Type Celled Grid') ,
      '#description' => t('A grid can have rows divisions only between simple cellede or internal rows') ,
      '#empty_value' => '',
      '#options' => array(
        'celled' => t('Celled') ,
        'internal' => t('Internal') ,
      ) ,
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[row_options][celled_grid]"]' => array(
            'checked' => FALSE,
          ) ,
        ) ,
        'required' => array(
          ':input[name="style_options[row_options][celled_grid]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['row_options']['type_celled_grid'],
    );

    // Responsive Grids Settings.
    $form['responsive_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Responsive Grid Options') ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[style_grid_type]"]' => array(
            'value' => 'responsive',
          ) ,
        ) ,
      ) ,
    );

    $form['responsive_options']['responsive_grids'] = array(
      '#type' => 'select',
      '#title' => t('Type Grids') ,
      '#options' => array(
        'wide' => t('Wide') ,
        'stackable' => t('Stackable') ,
        'doubling' => t('Doubling') ,
      ) ,
      '#empty_value' => '',
      '#default_value' => $this->options['responsive_options']['responsive_grids'],
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[style_grid_type]"]' => array(
            'value' => 'rows',
          ) ,
        ) ,
        'required' => array(
          ':input[name="style_options[style_grid_type]"]' => array(
            'value' => 'responsive',
          ) ,
        ) ,
      ) ,
    );

    // Global Grids Settings.
    $form['global_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Global Grid Options') ,
    );

    $form['global_options']['columns'] = array(
      '#type' => 'select',
      '#title' => t('Columns Count') ,
      '#options' => array(
        'one' => t('One') ,
        'two' => t('Two') ,
        'three' => t('Three') ,
        'four' => t('Four') ,
        'five' => t('Five') ,
        'six' => t('Six') ,
        'seven' => t('Seven') ,
        'eight' => t('Eight') ,
        'nine' => t('Nine') ,
        'ten' => t('Ten') ,
        'eleven' => t('Eleven') ,
        'twelve' => t('Twelve') ,
        'thirten' => t('Thirten') ,
        'fourten' => t('Fourten') ,
        'fiveten' => t('Fiveten') ,
        'sixten' => t('Sixten') ,
      ) ,
      '#required' => TRUE,
      '#default_value' => $this->options['global_options']['columns'],
    );

    $form['global_options']['customize_by_device'] = array(
      '#type' => 'checkbox',
      '#title' => t('Customize By Device') ,
      '#description' => t('A grid row can specify a width for a specific device') ,
      '#default_value' => $this->options['global_options']['customize_by_device'],
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[responsive_options][responsive_grids]"]' => array(
            '!value' => 'wide',
          ) ,
        ) ,
      ) ,
    );

    $form['global_options']['columns_mobile'] = array(
      '#type' => 'select',
      '#title' => t('Mobile Columns Count') ,
      '#options' => array(
        'one' => t('One') ,
        'two' => t('Two') ,
        'three' => t('Three') ,
        'four' => t('Four') ,
        'five' => t('Five') ,
        'six' => t('Six') ,
        'seven' => t('Seven') ,
        'eight' => t('Eight') ,
        'nine' => t('Nine') ,
        'ten' => t('Ten') ,
        'eleven' => t('Eleven') ,
        'twelve' => t('Twelve') ,
        'thirten' => t('Thirten') ,
        'fourten' => t('Fourten') ,
        'fiveten' => t('Fiveten') ,
        'sixten' => t('Sixten') ,
      ) ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[global_options][customize_by_device]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['global_options']['columns_mobile'],
    );

    $form['global_options']['columns_tablet'] = array(
      '#type' => 'select',
      '#title' => t('Tablet Columns Count') ,
      '#options' => array(
        'one' => t('One') ,
        'two' => t('Two') ,
        'three' => t('Three') ,
        'four' => t('Four') ,
        'five' => t('Five') ,
        'six' => t('Six') ,
        'seven' => t('Seven') ,
        'eight' => t('Eight') ,
        'nine' => t('Nine') ,
        'ten' => t('Ten') ,
        'eleven' => t('Eleven') ,
        'twelve' => t('Twelve') ,
        'thirten' => t('Thirten') ,
        'fourten' => t('Fourten') ,
        'fiveten' => t('Fiveten') ,
        'sixten' => t('Sixten') ,
      ) ,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[global_options][customize_by_device]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['global_options']['columns_tablet'],
    );

    $form['global_options']['grid_alignement'] = array(
      '#type' => 'select',
      '#title' => t('Alignment Grid') ,
      '#description' => t('A grid can specify alignment') ,
      '#empty_value' => '',
      '#options' => array(
        'left' => t('Left') ,
        'center' => t('Center') ,
        'right' => t('Right') ,
      ) ,
      '#default_value' => $this->options['global_options']['grid_alignement'],
    );

    $form['global_options']['vertical_alignement'] = array(
      '#type' => 'select',
      '#title' => t('Vertical Alignment') ,
      '#description' => t('A grid can specify its vertical alignment to have all its columns vertically centered.') ,
      '#empty_value' => '',
      '#options' => array(
        'top' => t('Top') ,
        'middle' => t('Middle') ,
        'bottom' => t('Left') ,
      ) ,
      '#default_value' => $this->options['global_options']['vertical_alignement'],
    );

    // Variations Settings.
    $form['variations_set'] = array(
      '#type' => 'fieldset',
      '#title' => t('Setting Variations') ,
    );

    $form['variations_set']['segment'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Segment per grid ?') ,
      '#description' => t('Content wrapped by segment') ,
      '#default_value' => $this->options['variations_set']['segment'],
      '#states' => array(
        'disabled' => array(
          array(
            ':input[name="style_options[row_options][celled_grid]"]' => array(
              'checked' => TRUE,
            ) ,
          ) ,
        ) ,
      ) ,
    );

    $form['variations_set']['padded'] = array(
      '#type' => 'checkbox',
      '#title' => t('Padded Grid') ,
      '#description' => t('A grid can preserve its vertical and horizontal gutters on first and last columns') ,
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[variations_set][relaxed]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['variations_set']['padded'],
    );

    $form['variations_set']['relaxed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Relaxed Grid') ,
      '#description' => t('A grid can increase its gutters to allow for more negative space') ,
      '#states' => array(
        'disabled' => array(
          ':input[name="style_options[variations_set][padded]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['variations_set']['relaxed'],
    );

    $form['variations_set']['divided_grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Divided Grid') ,
      '#description' => t('A grid can have dividers between its columns') ,
      '#states' => array(
        'checked' => array(
          ':input[name="style_options[row_options][equal_height]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
        'disabled' => array(
          array(
            ':input[name="style_options[row_options][celled_grid]"]' => array(
              'checked' => TRUE,
            ),
          ) ,
          array(
            ':input[name="style_options[row_options][equal_height]"]' => array(
              'checked' => TRUE,
            ),
          ) ,
        ) ,
        'required' => array(
          ':input[name="style_options[row_options][equal_height]"]' => array(
            'checked' => TRUE,
          ) ,
        ) ,
      ) ,
      '#default_value' => $this->options['variations_set']['divided_grid'],
    );

    $form['variations_set']['verticaly_grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Verticaly Divided Grid') ,
      '#description' => t('A grid can have dividers between rows') ,
      '#default_value' => $this->options['variations_set']['verticaly_grid'],
    );
  }

}

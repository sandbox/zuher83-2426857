<?php

/**
 * @file
 * Semantic_ui_views_plugin_grid data handler.
 */

/**
 * Implements hook_views_plugins().
 */
function semantic_ui_views_plugin_grid_views_plugins() {
  $module_path = drupal_get_path('module', 'semantic_ui_views_plugin_grid');

  return array(
    'style' => array(
      'semantic_ui_views_plugin_grid' => array(
        'title' => t('Semantic UI Grid') ,
        'help' => t('Semantic UI Grid') ,
        'path' => $module_path . '/plugins',
        'handler' => 'semantic_ui_views_plugin_grid',
        'parent' => 'default',
        'theme' => 'semantic_ui_views_plugin_grid',
        'theme path' => $module_path . '/templates',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'semantic-ui-views-plugin-grid',
      ) ,
    ) ,
  );
}
